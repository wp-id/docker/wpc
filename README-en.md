# WordPress Docker Stack

## Setup

1. Create a new directory for your project and initialize new git repository
    ```sh
    mkdir my-project
    cd my-project
    git init
    ```
1. Create a `.env` file
    ```sh
    echo "COMPOSE_PROJECT_NAME=my-project" > .env
    ```
1. Clone this repo as submodule of your project:
    ```sh
    git submodule add https://gitlab.com/wp-id/wpc wpc
    ```
1. Create database & content directories:
    ```sh
    mkdir -p services/db/data
    mkdir -p services/backend/wp-content
    ```
1. Copy the sample config:
    ```sh
    cp -r wpc/sample-env-config config
    # If you already have config directory, just copy the contents:
    # cp -r wpc/sample-env-config/* config/
    ```
1. Copy one of the sample override files:
    - With proxy: `cp config/local/docker-compose.override-proxy.yml config/local/docker-compose.override.yml`
    - Without proxy: `cp config/local/docker-compose.override-single.yml config/local/docker-compose.override.yml`
1. Add `127.0.0.1 wp.local` to `/etc/hosts`
1. Bring the containers up
    ```sh
    ./wpc/bin/up -e local
    ```
1. Wait until you see these lines on your terminal:
    ```sh
    wp.local-backend | 2018-04-17 03:32:19,762 NOTICE: fpm is running, pid 1
    wp.local-backend | 2018-04-17 03:32:19,762 NOTICE: ready to handle connections
    ```
    ... open http://wp.local and continue with WordPress installation.
1. To bring down the containers:
    ```sh
    ./wpc/bin/down
    ```

### Setup Notes

-   You most likely want to add these entries to your project's `.gitignore` file:
    ```sh
    services/db/data
    services/backend/wp-content/uploads
    services/backend/wp-content/upgrade
    ```
-   To import a database dump:
    ```sh
    ./wpc/bin/db-import /path/to/db.sql
    ```
-   To create a database dump:
    ```sh
    ./wpc/bin/db-export /path/to/db.sql
    ```
