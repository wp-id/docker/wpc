# WordPress Docker Stack

View readme in [English](README-en.md).

## Persiapan

1. Buat directory baru untuk proyek anda, lalu siapkan git
    ```sh
    mkdir my-project
    cd my-project
    git init
    ```
1. Buat berkas `.env`
    ```sh
    echo "COMPOSE_PROJECT_NAME=my-project" > .env
    ```
1. Clone repositori ini sebagai submodule proyek anda:
    ```sh
    git submodule add https://gitlab.com/wp-id/wpc wpc
    ```
1. Buat direktori untuk database & content:
    ```sh
    mkdir -p services/db/data
    mkdir -p services/backend/wp-content
    ```
1. Salin berkas pengaturan lalu sesuaikan:
    ```sh
    cp -r wpc/sample-env-config config
    # Jika anda sudah memiliki direktori config, cukup salin isinya saja:
    # cp -r wpc/sample-env-config/* config/
    ```
1. Salin salah satu berkas override:
    - Dengan proxy: `cp config/local/docker-compose.override-proxy.yml config/local/docker-compose.override.yml`
    - Tanpa proxy: `cp config/local/docker-compose.override-single.yml config/local/docker-compose.override.yml`
1. Tambahkan `127.0.0.1 wp.local` ke `/etc/hosts`
1. Jalankan container
    ```sh
    ./wpc/bin/up -e local
    ```
1. Tunggu sampai anda melihat baris seperti di bawah ini di terminal anda:
    ```sh
    wp.local-backend | 2018-04-17 03:32:19,762 NOTICE: fpm is running, pid 1
    wp.local-backend | 2018-04-17 03:32:19,762 NOTICE: ready to handle connections
    ```
    ... lalu buka http://wp.local dan lanjutkan dengan pemasangan WordPress.
1. Untuk menghentikan container:
    ```sh
    ./wpc/bin/down
    ```

### Catatan

-   Disarankan untuk menambahkan berkas-berkas berikut ke dalam berkas `.gitignore` proyek anda:
    ```sh
    services/db/data
    services/backend/wp-content/uploads
    services/backend/wp-content/upgrade
    ```
-   Untuk mengimpor database:
    ```sh
    ./wpc/bin/db-import /path/to/db.sql
    ```
-   Untuk mengkespor:
    ```sh
    ./wpc/bin/db-export /path/to/db.sql
    ```
