#!/bin/sh

set -e
source "$(dirname "$0")/helpers"

if [ -z "${1+x}" ]; then
	echo 'Please provide image name ;-)'
	exit 1;
fi

SCRIPT_DIR_PATH="$(get_script_dir_path)"
PROJECT_DIR_PATH="$(get_project_dir_path)"

if [ -e "${PROJECT_DIR_PATH}/composer.json" ]; then
	echo "=> Installing composer packages..."
	${SCRIPT_DIR_PATH}/composer install --no-dev
fi

echo -e "\n=> Building image ${1} ..."
docker build -t "${1}" "${PROJECT_DIR_PATH}/services/backend"

echo "Done!"
